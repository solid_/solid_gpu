# solid_ GPU farm

The following repository aggregates all scripts and configurations needed to run a equihash farming unit.

## Folders

* ```configs/``` - Miner configurations for zcash and zclassic.
* ```scripts/``` - Scripts for miner startup and fan speed control.
* ```services/``` - Operative System Services. Monitored by systemd.
* ```src/``` - Binaries.